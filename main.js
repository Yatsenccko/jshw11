function submit(event) {
    const inputs = document.querySelectorAll('.inputs')
    const btn = document.querySelector('.btn')
    const pErorr = document.querySelector('.pErorr')


    btn.addEventListener('click', () => {
        if (inputs[0].value != inputs[1].value) {
            pErorr.classList.add('active')
        } else {
            alert('You are welcome')
            pErorr.classList.remove('active')
        }

    })
}
function linkF(eye) {
    const eyeId = eye.id
    const inputId = 'input' + eyeId
    const input = document.getElementById(inputId)
    eye.addEventListener('click', () => {
        if (input.type === "text") {
            input.type = "password"
        } else {
            input.type = "text"
        }
        eye.classList.toggle('fa-eye-slash')

    })
}

function linkAll() {
    let eyes = document.querySelectorAll('.icon-password')
    eyes.forEach(element => linkF(element))
}
submit()
linkAll()